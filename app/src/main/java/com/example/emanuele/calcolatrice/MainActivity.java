package com.example.emanuele.calcolatrice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.icu.math.*;

public class MainActivity extends AppCompatActivity {
    EditText input=null;
    String operator ="+";
    double temp=0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input=(EditText)findViewById(R.id.input);
    }

    public void append(View v){
        Button b = (Button)v;
        if (input.getText().toString().equals("0")){
            input.setText(b.getText());
        }else {
            input.getText().append(b.getText());
        }
    }

    public void clear(View v){
        input.setText("0");
        operator="+";
        temp=0;
    }

    public void operation(View v){
        String nextOperator = ((Button)v).getText().toString();
        Double numero = Double.parseDouble(input.getText().toString());

        switch (operator){
            case "+":
                temp+=numero;
                break;
            case "-":
                temp-=numero;
                break;
            case "*":
                temp*=numero;
                break;
            case "/":
                temp/=numero;
                break;

            case "sin(x)":
                if(numero==180 || numero==360)
                    temp=0;
                else
                    temp=Math.sin((numero*Math.PI)/180);
                break;

            case "cos(x)":
                if(numero==90 || numero==270)
                    temp=0;
                else
                    temp=Math.cos((numero*Math.PI)/180);
                break;

            case "tan(x)":
                if(numero==180 || numero==360)
                    temp=0;
                else
                    temp=Math.tan((numero*Math.PI)/180);
                break;

            case "cot(x)":
                if(numero==90 || numero==180)
                    temp=0;
                else
                    temp=1/Math.tan((numero*Math.PI)/180);
                break;

            case "ln":
                temp=Math.log(numero);
                break;

            case "√":
                temp=Math.sqrt(numero);
                break;

            case "x²":
                temp=Math.pow(numero, 2);
                break;

            case "x³":
                temp=Math.pow(numero, 3);
                break;
        }

        if (nextOperator.equals("=")){
            input.setText(String.valueOf(temp));
        }else {
            input.setText("0");
        }

        operator=nextOperator;
    }
}
